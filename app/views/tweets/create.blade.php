@extends('layouts.scaffold')

@section('main')

<h1>Create Tweet</h1>

{{ Form::open(array('route' => 'tweets.store')) }}
	<ul>
        <li>
            {{ Form::label('body', 'Body:') }}
            {{ Form::textarea('body') }}
        </li>

        <li>
            {{ Form::label('person_id', 'Person:') }}
            {{ Form::select('person_id', $people) }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop