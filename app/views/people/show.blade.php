@extends('layouts.scaffold')

@section('main')

<h1>Show Person</h1>

<p>{{ link_to_route('people.index', 'Return to all people') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
			<th>Bio</th>
			<th>Location</th>
			<th>Tweet Count</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $person->name }}}</td>
			<td>{{{ $person->bio }}}</td>
			<td>{{{ $person->location }}}</td>
			<td>{{{ count($person->tweets) }}}</td>
            <td>{{ link_to_route('people.edit', 'Edit', array($person->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('people.destroy', $person->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
