<?php

class Person extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'bio' => 'required',
		'location' => 'required'
	);

	public function tweets() {
		return $this->hasMany('Tweet');
	}

	public static function getSelectList() {
		$people = Person::all();

		$out = array();
		foreach ($people as $person) {
			$out[$person->id] = $person->name;
		}

		return $out;
	}
}
