<?php

class Tweet extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'body' => 'required',
		'person_id' => 'required'
	);

	public function person() {
		return $this->belongsTo('Person');
	}
}
